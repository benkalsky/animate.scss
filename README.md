# Animate.scss

This is a port of Dan Eden's [Animate.css](http://daneden.github.io/animate.css/) for SCSS.

## Doesn't this already exist somewhere else?

Yes, there are plenty of other ports of this library. Many of them aren't very active projects and, honestly, I was just a little too lazy to submit my changes to those repositories.

Also, I was looking for something a little more flexible. This version allows you to import all animations at a whopping 53kb or only import what you need. It's a flavor thing.

## Installing

The default import to include animations. 

```
@import 'animate';
```

Want to pick and choose which animations are imported? Go into animate.scss and comment out what you don't need.

```
// Always required
@import 'properties';

// Import the animations (Comment to uninclude)
@import 'attention-seekers/bounce';
@import 'attention-seekers/flash';
@import 'attention-seekers/pulse';
@import 'attention-seekers/rubberBand';
@import 'attention-seekers/shake';
@import 'attention-seekers/swing';
@import 'attention-seekers/tada';
@import 'attention-seekers/wobble';

@import 'bouncing-entrances/bounceIn';
@import 'bouncing-entrances/bounceInDown';
@import 'bouncing-entrances/bounceInLeft';
@import 'bouncing-entrances/bounceInRight';
@import 'bouncing-entrances/bounceInUp';

@import 'bouncing-exits/bounceOut';
@import 'bouncing-exits/bounceOutDown';
@import 'bouncing-exits/bounceOutLeft';
@import 'bouncing-exits/bounceOutRight';
@import 'bouncing-exits/bounceOutUp';

@import 'fading-entrances/fadeIn';
@import 'fading-entrances/fadeInDown';
@import 'fading-entrances/fadeInDownBig';
@import 'fading-entrances/fadeInLeft';
@import 'fading-entrances/fadeInLeftBig';
@import 'fading-entrances/fadeInRight';
@import 'fading-entrances/fadeInRightBig';
@import 'fading-entrances/fadeInUp';
@import 'fading-entrances/fadeInUpBig';

@import 'fading-exits/fadeOut';
@import 'fading-exits/fadeOutDown';
@import 'fading-exits/fadeOutDownBig';
@import 'fading-exits/fadeOutLeft';
@import 'fading-exits/fadeOutLeftBig';
@import 'fading-exits/fadeOutRight';
@import 'fading-exits/fadeOutRightBig';
@import 'fading-exits/fadeOutUp';
@import 'fading-exits/fadeOutUpBig';

@import 'flippers/flip';
@import 'flippers/flipInX';
@import 'flippers/flipInY';
@import 'flippers/flipOutX';
@import 'flippers/flipOutY';

@import 'lightspeed/lightSpeedIn';
@import 'lightspeed/lightSpeedOut';

@import 'rotating-entrances/rotateIn';
@import 'rotating-entrances/rotateInDownLeft';
@import 'rotating-entrances/rotateInDownRight';
@import 'rotating-entrances/rotateInUpLeft';
@import 'rotating-entrances/rotateInUpRight';

@import 'rotating-exits/rotateOut';
@import 'rotating-exits/rotateOutDownLeft';
@import 'rotating-exits/rotateOutDownRight';
@import 'rotating-exits/rotateOutUpLeft';
@import 'rotating-exits/rotateOutUpRight';

@import 'sliding-entrances/slideInDown';
@import 'sliding-entrances/slideInLeft';
@import 'sliding-entrances/slideInRight';
@import 'sliding-entrances/slideInUp';

@import 'sliding-exits/slideOutDown';
@import 'sliding-exits/slideOutLeft';
@import 'sliding-exits/slideOutRight';
@import 'sliding-exits/slideOutUp';

@import 'specials/hinge';
@import 'specials/rollIn';
@import 'specials/rollOut';

@import 'zooming-entrances/zoomIn';
@import 'zooming-entrances/zoomInDown';
@import 'zooming-entrances/zoomInLeft';
@import 'zooming-entrances/zoomInRight';
@import 'zooming-entrances/zoomInUp';

@import 'zooming-exits/zoomOut';
@import 'zooming-exits/zoomOutDown';
@import 'zooming-exits/zoomOutLeft';
@import 'zooming-exits/zoomOutRight';
@import 'zooming-exits/zoomOutUp';
```

## Usage

Once your files have been added to your project and you've customized your `@imports`, you can start including the animations directly to your classes.

```
.your-class-name {
  @include bounceIn();
}
```

The mixin includes configurable options to customize the `delay`, `count` `duration`, `function` and `fill-mode` of your animations.

```
.your-class-name {
  @include bounceIn($duration: 1s, $count: 2, $delay: .2s, $function: ease, $fill: both);
}
```

## Licenses

Animate.css and Animate.scss are both licensed under the MIT license. (http://opensource.org/licenses/MIT)
